<?php
class db
{

	private $dbhost = 'localhost';

	// Shanira server
	private $dbname = 'shanira_chat';
	private $dbuser = 'shanira_appchat';
	private $dbpass = 'Duck@a11';

	// Local Server
	// private $dbname = 'chatapp';
	// private $dbuser = 'root';
	// private $dbpass = '';


	public function connect()
	{
		$mysql_conn_str = "mysql:host=$this->dbhost;dbname=$this->dbname";
		$dbconnection = new PDO($mysql_conn_str, $this->dbuser, $this->dbpass);
		$dbconnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$dbconnection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
		return $dbconnection;
	}
}
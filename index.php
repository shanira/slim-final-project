<?php
	use \Psr\Http\Message\ServerRequestInterface as Request;
	use \Psr\Http\Message\ResponseInterface as Response;
	use \Firebase\JWT\JWT;

	require __DIR__ .'/vendor/autoload.php';
	require __DIR__ .'/config/db.php';
	require __DIR__ .'/config/file.php';

	$config['displayErrorDetails'] = true;
	$config['addContentLengthHeader'] = false;

	$dotenv = new Dotenv\Dotenv(__DIR__);
	$dotenv->load();
	$app = new \Slim\App(["setings" => $config]);

	$app->options('/{routes:.+}', function ($request, $response, $args){
	    return $response;
	});

	// Allowing  other apps to use slim
		$app->add(function ($req, $res, $next){
		    $response = $next($req, $res);
		    return $response
		            ->withHeader('Access-Control-Allow-Origin', '*')
		            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
		            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
		});

	// Authentication method for users
		$authenticate = function($request, $response, $next) {
			$header = $request->getHeaders();
			$result = array();
			if ($request->hasHeader('HTTP_AUTHORIZATION')) {
				try {
					$header = $request->getHeaders();
					//$header = substr($header['HTTP_AUTHORIZATION'][0],7);
					$secretKey = base64_decode(getenv("JWT_SECRET"));
					$DecodedDataArray = JWT::decode(substr($header['HTTP_AUTHORIZATION'][0],7), $secretKey, array(getenv("ALGORITHM")));
					$result['error'] = false;
					$result['data'] = json_encode($DecodedDataArray);
					$response = $next($request, $response);
					return $response;
				} catch (Exception $e) {
					return $response->withStatus(401)->write('Unauthorized, wrong token.');
				}
			} else {
					return $response->withStatus(401)->write('Unauthorized, token missing.');
				}
		};

// Test the slim works
	$app->get('/api/test', function(Request $request, Response $response) {
		$response = "test successful !";
		// $headers = apache_request_headers();
		// var_dump($headers);
		return $response;
	});//->add($authenticate);
// Login for a user
	$app->post('/api/login', function(Request $request, Response $response) {	
		$email = $request->getParam('email');
		$password = $request->getParam('password');
		$sql = "SELECT password FROM users WHERE email = :email LIMIT 1";

		try {
			$db = new db();
			$db = $db->connect();
			$stmt = $db->prepare($sql);
			$stmt->bindParam(':email', $email);
			$stmt->execute();
			$row_login = $stmt->fetch();
			// $db = null;
			$db_psw = $row_login['password'];

			if (password_verify($password, $db_psw)) {
				$data = "SELECT full_name, display_name, email FROM users WHERE email = :email LIMIT 1";
				$stmt = $db->prepare($data);
				$stmt->bindParam(':email', $email);
				$stmt->execute();
				$row_userdata = $stmt->fetch();
				$name = $row_userdata['full_name'];
				$display_name = $row_userdata['display_name'];
				$email = $row_userdata['email'];
				$db = null;

			// Token generation fron firebase JWT
				$tokenId    = base64_encode(mcrypt_create_iv(32));
				$issuedAt   = time();
				$notBefore  = $issuedAt;
				$expire     = $notBefore + 72000;
				$serverName = 'http://shanira.myweb.jce.ac.il/chat/';
				$data = [
						'iat'  => $issuedAt,
						'jti'  => $tokenId,
						'iss'  => $serverName,
						'nbf'  => $notBefore,
						'exp'  => $expire,
						'data' => [
								//'id'   => 1,
								'name' => $name,
								'email' => $email
								]
						];
				$secretKey = base64_decode(getenv("JWT_SECRET"));
				$jwt = JWT::encode($data, $secretKey, getenv("ALGORITHM")); 

				$encodedArray = ['jwt' => $jwt];
				$result = array();
				$result['error'] = false;
				$result['name'] = $row_userdata['full_name'];
				$result['email'] = $row_userdata['email'];
				$result['display_name'] = $row_userdata['display_name'];
				$result['token'] = $jwt;
			} else {
				$result['error'] = true;
				$result['message'] = 'Wrong password!';
			}
			if (!$row_login) {
				$result['error'] = true;
				$result['message'] = 'Wrong Email address !';
			}
			return $this->response->withJson($result);
		} catch (Exception $e) {
			echo '{"error" : {"text" : '.$e->getMessage().'}';
		}
	});

// get all users
	$app->get('/api/users', function(Request $request, Response $response){
		$sql = "SELECT full_name, display_name, phone, email, address, city, state FROM users";

		try {
			$db = new db();
			$db = $db->connect();

			$stmt = $db->query($sql);
			$users = $stmt->fetchAll(PDO::FETCH_OBJ);
			$db = null;
			return json_encode($users);
		} catch (Exception $e) {
			echo '{"error" : {"text" : '.$e->getMessage().'}';
			
		}
	})->add($authenticate);

// get user info
	$app->get('/api/userinfo/{displayName}', function(Request $request, Response $response){	
		$displayName = $request->getAttribute('displayName');
		$sql = "SELECT id, full_name, display_name, phone, email, address, city, state FROM users WHERE display_name = :displayName";
		try {
			$db = new db();
			$db = $db->connect();
			$stmt = $db->prepare($sql);
			$stmt->bindParam(':displayName', $displayName);
			$stmt->execute();
			$user = $stmt->fetch(PDO::FETCH_OBJ);
			$db = null;
			echo json_encode($user);
			//echo "Not exist";
		} catch (Exception $e) {
			echo '{"error" : {"text" : '.$e->getMessage().'}';
		}
	})->add($authenticate);

// create new user
	$app->post('/api/user/add', function(Request $request, Response $response){	
		$full_name = $request->getParam('full_name');
		$display_name = $request->getParam('display_name');
		$phone = $request->getParam('phone');
		$email = $request->getParam('email');
		$address = $request->getParam('address');
		$password = $request->getParam('password');
		$hashed_password = password_hash($password, PASSWORD_DEFAULT);
		$city = $request->getParam('city');
		$state = $request->getParam('state');
		$sql = "INSERT INTO users (full_name, display_name, phone, email, password, address, city, state) VALUES (:full_name, :display_name, :phone, :email, :password, :address, :city, :state)";
		try {
			$db = new db();
			$db = $db->connect();
			$stmt = $db->prepare($sql);
			$stmt->bindParam(':full_name', $full_name);
			$stmt->bindParam(':display_name', $display_name);
			$stmt->bindParam(':phone', $phone);
			$stmt->bindParam(':email', $email);
			$stmt->bindParam(':password', $hashed_password);
			$stmt->bindParam(':address', $address);
			$stmt->bindParam(':city', $city);
			$stmt->bindParam(':state', $state);
			$stmt->execute();
			/***
				To add a user :
				{"full_name":"Brad",
			    "display_name":"Hussy",
			    "phone":"880-171-454-658",
			    "email":"bradhussy@gmail.com",
			    "address":"a4/99, Calf",
			    "city":"Bogra",
				"state":"Rajshahi",
				"password" : "132546"}
			***/
			echo '{"notice": {"text": "user Added"}';
		} catch (Exception $e) {
			echo '{"error" : {"text" : '.$e->getMessage().'}';
		}
	});

// upload photo
	$app->post('/api/upload/{displayName}', function (Request $request, Response $response){
		$displayName = $request->getAttribute('displayName');
		if (!isset($_FILES['uploads'])) {
	        echo "Select a file !";
	        return false;
		}
		$uploaddir = $_SERVER["DOCUMENT_ROOT"]."/server/assets/";
	    $files = $_FILES['uploads'];
		$filename = $files['name'];
		$name = uniqid('img-'.date('Ymd')).$filename;
		$uploadfile = $uploaddir.$name;

		if (move_uploaded_file($files['tmp_name'], $uploadfile)) {
			$sql = "UPDATE users SET photo = :photo_url WHERE display_name = :displayName";;
			try {
				$db = new db();
				$db = $db->connect();
				$stmt = $db->prepare($sql);
				$stmt->bindParam(':displayName', $displayName);
				$stmt->bindParam(':photo_url', $name);
				$stmt->execute();
				$db = null;
				echo "Successfully Uploaded !";
			} catch (Exception $e) {
				echo '{"error" : {"text" : '.$e->getMessage().'}}';
			}
		}else{
			echo "Sorry Upload failed !";
		}
	})->add($authenticate);

// get photo of user
	$app->get('/api/propic/{displayName}', function(Request $request, Response $response){	
		$displayName = $request->getAttribute('displayName');
		$sql = "SELECT photo FROM users WHERE display_name = :displayName";
		try {
			$db = new db();
			$db = $db->connect();
			$stmt = $db->prepare($sql);
			$stmt->bindParam(':displayName', $displayName);
			$stmt->execute();
			$photoName = $stmt->fetch(PDO::FETCH_OBJ);
			$db = null;
			echo json_encode($photoName);
		} catch (Exception $e) {
			echo '{"error" : {"text" : '.$e->getMessage().'}';
		}
	})->add($authenticate);

// get a photo
	$app->get('/api/photo/{photoName}',function($request, $response){
		$photoName = $request->getAttribute('photoName');
		$filePath = $_SERVER["DOCUMENT_ROOT"]."/server/assets/".$photoName.".jpg";
		echo $filePath;
		return moshiur\fileResponse\FileResponse::getResponse($response, $filePath);
	});

// get default photo
	$app->get('/api/photo/',function($request, $response){
		$filePath = $_SERVER["DOCUMENT_ROOT"]."/server/assets/default.jpg";
		echo $filePath;
		return moshiur\fileResponse\FileResponse::getResponse($response, $filePath);
	});

// update user
	$app->post('/api/user/update/{displayname}', function(Request $request, Response $response){	
		$display_name = $request->getAttribute('displayname');
		$full_name = $request->getParam('full_name');
		$phone = $request->getParam('phone');
		$email = $request->getParam('email');
		$address = $request->getParam('address');
		$city = $request->getParam('city');
		$state = $request->getParam('state');

		$sql = "UPDATE users SET full_name = :full_name, phone = :phone, email = :email, address = :address, city = :city, state = :state	
				WHERE display_name = :display_name";

		try {
			$db = new db();
			$db = $db->connect();
			$stmt = $db->prepare($sql);
			$stmt->bindParam(':full_name', $full_name);
			$stmt->bindParam(':phone', $phone);
			$stmt->bindParam(':email', $email);
			$stmt->bindParam(':address', $address);
			$stmt->bindParam(':city', $city);
			$stmt->bindParam(':state', $state);

			$stmt->bindParam(':display_name', $display_name);
			$stmt->execute();
			echo '{"notice": {"text": "user Updated"}}';

		} catch (Exception $e) {
			echo '{"notice" : {"text" : '.$e->getMessage().'}';
		}
	})->add($authenticate);

// delete user
	$app->delete('/api/user/delete/{id}', function(Request $request, Response $response){	
		$id = $request->getAttribute('id');
		$sql = "DELETE FROM users WHERE id = $id";

		try {
			$db = new db();
			$db = $db->connect();
			$stmt = $db->prepare($sql);
			$stmt->execute();
			$db = null;
			echo '{"notice": {"text": "user Deleted"}}';
		} catch (Exception $e) {
			echo '{"error" : {"text" : '.$e->getMessage().'}';
		}
	})->add($authenticate);

	$app->run();
?>